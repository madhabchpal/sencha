Ext.define('myMobileApp.view.Main', {
    extend: 'Ext.tab.Panel',
    
    config: {
		fullscreen: true,
		items: [{
			title: 'Home',
			iconCls: 'home',
			html: 'Welcome'
		},{
			title: 'Settings',
			iconCls: 'settings',
			html: 'Settings'
		}]
    }
});